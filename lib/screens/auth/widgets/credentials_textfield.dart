import 'package:flutter/material.dart';

class LoginTextField extends StatelessWidget {
  final String hintText;

  const LoginTextField({Key? key, required this.hintText}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 10),
      child: TextField(
        decoration: InputDecoration(
          border: OutlineInputBorder(
            borderRadius: const BorderRadius.all(
              const Radius.circular(30.0),
            ),
            borderSide: BorderSide(
              color: Colors.grey.shade300,
            ),
          ),
          hintStyle: TextStyle(
            color: Colors.grey,
          ),
          fillColor: Colors.black,
          hintText: hintText,
        ),
      ),
    );
  }
}
