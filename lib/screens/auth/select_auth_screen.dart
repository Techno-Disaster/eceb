import 'package:eceb/screens/auth/widgets/banners.dart';
import 'package:eceb/screens/auth/widgets/custom_select_button.dart';
import 'package:eceb/utils/router.dart';
import 'package:flutter/material.dart';

class SelectAuthScreen extends StatelessWidget {
  final GlobalKey navBarKey;

  const SelectAuthScreen({Key? key, required this.navBarKey}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          Hero(
            tag: "banner",
            child: Material(
              child: Banner1(),
            ),
          ),
          SizedBox(
            height: 40,
          ),
          CustomSelectButton(
            title: "Individual",
            onPressed: () => Navigator.pushNamed(context, individualLogin,
                arguments: navBarKey),
          ),
          SizedBox(
            height: 40,
          ),
          CustomSelectButton(
            title: "Facility",
            onPressed: () => Navigator.pushNamed(context, facilityLogin,
                arguments: navBarKey),
          ),
          SizedBox(
            height: 40,
          ),
        ],
      ),
    );
  }
}
