import 'package:eceb/screens/home/home_screen.dart';
import 'package:eceb/screens/home/list_of_babies_screen.dart';
import 'package:eceb/screens/home/notifications_screen.dart';
import 'package:eceb/screens/home/profile_screen.dart';
import 'package:eceb/screens/home/widgets/custom_app_drawer.dart';
import 'package:flutter/material.dart';

class Base extends StatefulWidget {
  final GlobalKey navBarKey;

  const Base({Key? key, required this.navBarKey}) : super(key: key);
  @override
  _BaseState createState() => _BaseState();
}

class _BaseState extends State<Base> {
  int _currentIndex = 0;
  late final List<Widget> _children;
  @override
  void initState() {
    _children = [
      HomeScreen(navBarKey: widget.navBarKey),
      ListofBabiesScreen(),
      NotificationsScreen(),
      ProfileScreen(),
    ];
    super.initState();
  }

  void _onSelected(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        preferredSize:
            _currentIndex == 2 ? Size.fromHeight(0) : Size.fromHeight(80),
        child: _currentIndex == 2
            ? Container() //custom tabbar view for notification page declared later
            : CustomAppBar(
                title: _currentIndex == 1 ? "List of Babies" : "ECEB",
                drawerAvailable: _currentIndex != 0 ? false : true,
              ),
      ),
      body: _children[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        key: widget.navBarKey,
        selectedItemColor: Theme.of(context).primaryColor,
        unselectedItemColor: Colors.grey,
        onTap: _onSelected,
        currentIndex: _currentIndex,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.list),
            label: 'List of Babies',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.notifications_active),
            label: 'Notifications',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: 'Profile',
          )
        ],
      ),
    );
  }
}
