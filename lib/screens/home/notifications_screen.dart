import 'package:eceb/utils/router.dart';
import 'package:flutter/material.dart';

class NotificationsScreen extends StatefulWidget {
  @override
  _NotificationsScreenState createState() => _NotificationsScreenState();
}

class _NotificationsScreenState extends State<NotificationsScreen>
    with SingleTickerProviderStateMixin {
  late TabController tabController;
  final List<Tab> myTabs = <Tab>[
    Tab(text: 'Risk Assesment'),
    Tab(text: 'Monitoring Alerts'),
  ];
  @override
  void initState() {
    tabController = TabController(length: myTabs.length, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: 80,
          decoration: BoxDecoration(
            color: Theme.of(context).primaryColor,
            borderRadius: BorderRadius.only(
              bottomLeft: const Radius.circular(25.0),
              bottomRight: const Radius.circular(25.0),
            ),
          ),
          child: TabBar(
            tabs: myTabs,
            controller: tabController,
            indicator: UnderlineTabIndicator(
              borderSide: BorderSide(
                color: Colors.white,
                width: 4.0,
              ),
              insets: EdgeInsets.symmetric(horizontal: 24.0),
            ),
            labelColor: Colors.white,
          ),
        ),
        Expanded(
          child: TabBarView(
            children: [
              RiskAssesmenttList(),
              MonitoringAlertstList(),
            ],
            controller: tabController,
          ),
        ),
      ],
    );
  }
}

class RiskAssesmenttList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(24, 12, 0, 12),
          child: Text(
            "Today",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 18,
            ),
          ),
        ),
        RiskAssesmentTile(
          name: "Oni",
          ward: "Prental",
          eatAt: "11:06 AM",
          previousStatus: "Problem",
          newStatus: "Normal",
          tilePrimaryColor: Colors.green,
          checkboxValue: true,
        ),
        RiskAssesmentTile(
          name: "Ada",
          ward: "Prental",
          eatAt: "10:56 AM",
          previousStatus: "Problem",
          newStatus: "Danger",
          tilePrimaryColor: Colors.red,
        ),
        RiskAssesmentTile(
          name: "Teka",
          ward: "Prental",
          eatAt: "11:06 AM",
          previousStatus: "Normal",
          newStatus: "Problem",
          tilePrimaryColor: Colors.yellow,
          checkboxValue: true,
        ),
        RiskAssesmentTile(
          name: "Evi",
          ward: "Prental",
          eatAt: "11:06 AM",
          previousStatus: "Problem",
          newStatus: "Normal",
          tilePrimaryColor: Colors.green,
          checkboxValue: true,
        ),
        RiskAssesmentTile(
          name: "Riya",
          ward: "Prental",
          eatAt: "11:06 AM",
          previousStatus: "Problem",
          newStatus: "Danger",
          tilePrimaryColor: Colors.red,
        ),
        RiskAssesmentTile(
          name: "Adrea",
          ward: "Prental",
          eatAt: "11:06 AM",
          previousStatus: "Normal",
          newStatus: "Problem",
          tilePrimaryColor: Colors.yellow,
        ),
        RiskAssesmentTile(
          name: "Oni",
          ward: "Prental",
          eatAt: "11:06 AM",
          previousStatus: "Problem",
          newStatus: "Normal",
          tilePrimaryColor: Colors.green,
        ),
        RiskAssesmentTile(
          name: "Ada",
          ward: "Prental",
          eatAt: "10:56 AM",
          previousStatus: "Problem",
          newStatus: "Danger",
          tilePrimaryColor: Colors.red,
        ),
        RiskAssesmentTile(
          name: "Teka",
          ward: "Prental",
          eatAt: "11:06 AM",
          previousStatus: "Normal",
          newStatus: "Problem",
          tilePrimaryColor: Colors.yellow,
          checkboxValue: true,
        ),
        RiskAssesmentTile(
          name: "Evi",
          ward: "Prental",
          eatAt: "11:06 AM",
          previousStatus: "Problem",
          newStatus: "Normal",
          tilePrimaryColor: Colors.green,
        ),
        RiskAssesmentTile(
          name: "Riya",
          ward: "Prental",
          eatAt: "11:06 AM",
          previousStatus: "Problem",
          newStatus: "Danger",
          tilePrimaryColor: Colors.red,
          checkboxValue: true,
        ),
        RiskAssesmentTile(
          name: "Adrea",
          ward: "Prental",
          eatAt: "11:06 AM",
          previousStatus: "Normal",
          newStatus: "Problem",
          tilePrimaryColor: Colors.yellow,
          checkboxValue: true,
        ),
      ],
    );
  }
}

class MonitoringAlertstList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(24, 12, 0, 12),
          child: Text(
            "Today",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 18,
            ),
          ),
        ),
        MonitoringAlertTile(
          name: "Oni",
          ward: "Prental",
          eatAt: "11:06 AM",
          duration: "Post 90 min",
          tilePrimaryColor: Colors.yellow,
          checkboxValue: true,
        ),
        MonitoringAlertTile(
          name: "Ada",
          ward: "Prental",
          eatAt: "11:06 AM",
          duration: "25-30 min",
          tilePrimaryColor: Colors.red,
        ),
        MonitoringAlertTile(
          name: "Oni",
          ward: "Prental",
          eatAt: "11:06 AM",
          duration: "Post 90 min",
          tilePrimaryColor: Colors.yellow,
          checkboxValue: true,
        ),
        MonitoringAlertTile(
          name: "Ada",
          ward: "Prental",
          eatAt: "11:06 AM",
          duration: "25-30 min",
          tilePrimaryColor: Colors.red,
        ),
        MonitoringAlertTile(
          name: "Oni",
          ward: "Prental",
          eatAt: "11:06 AM",
          duration: "Post 90 min",
          tilePrimaryColor: Colors.yellow,
          checkboxValue: true,
        ),
        MonitoringAlertTile(
          name: "Oni",
          ward: "Prental",
          eatAt: "11:06 AM",
          duration: "Post 90 min",
          tilePrimaryColor: Colors.yellow,
          checkboxValue: true,
        ),
        MonitoringAlertTile(
          name: "Oni",
          ward: "Prental",
          eatAt: "11:06 AM",
          duration: "Post 90 min",
          tilePrimaryColor: Colors.yellow,
          checkboxValue: true,
        ),
        MonitoringAlertTile(
          name: "Oni",
          ward: "Prental",
          eatAt: "11:06 AM",
          duration: "Post 90 min",
          tilePrimaryColor: Colors.yellow,
          checkboxValue: true,
        ),
        MonitoringAlertTile(
          name: "Oni",
          ward: "Prental",
          eatAt: "11:06 AM",
          duration: "Post 90 min",
          tilePrimaryColor: Colors.yellow,
          checkboxValue: true,
        ),
        MonitoringAlertTile(
          name: "Ada",
          ward: "Prental",
          eatAt: "11:06 AM",
          duration: "25-30 min",
          tilePrimaryColor: Colors.red,
        ),
        MonitoringAlertTile(
          name: "Oni",
          ward: "Prental",
          eatAt: "11:06 AM",
          duration: "Post 90 min",
          tilePrimaryColor: Colors.yellow,
          checkboxValue: true,
        ),
        MonitoringAlertTile(
          name: "Oni",
          ward: "Prental",
          eatAt: "11:06 AM",
          duration: "Post 90 min",
          tilePrimaryColor: Colors.yellow,
          checkboxValue: true,
        ),
      ],
    );
  }
}

class RiskAssesmentTile extends StatefulWidget {
  final String name;
  final String ward;
  final String eatAt;
  final String previousStatus;
  final String newStatus;
  final Color tilePrimaryColor;
  final bool checkboxValue;
  RiskAssesmentTile({
    Key? key,
    required this.name,
    required this.ward,
    required this.eatAt,
    required this.tilePrimaryColor,
    required this.previousStatus,
    required this.newStatus,
    this.checkboxValue = false, //default value for checkbox
  }) : super(key: key);

  @override
  _RiskAssesmentTileState createState() =>
      _RiskAssesmentTileState(checkboxValue);
}

class _RiskAssesmentTileState extends State<RiskAssesmentTile> {
  bool _checkBoxValue;
  _RiskAssesmentTileState(this._checkBoxValue);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ListTile(
          onTap: () => Navigator.pushNamed(context, babyDetails),
          leading: CircleAvatar(
            backgroundColor: widget.tilePrimaryColor,
          ),
          title: Text("Baby of ${widget.name}"),
          subtitle: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(
                "${widget.ward} ward",
                style: TextStyle(
                  fontSize: 13,
                ),
              ),
              Text(
                "EAT ${widget.eatAt}",
                style: TextStyle(
                  fontSize: 13,
                ),
              ),
            ],
          ),
          trailing: Container(
            width: MediaQuery.of(context).size.width * 0.5,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 12),
                  child: Column(
                    children: [
                      Text(
                        "Status changed:",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 13,
                        ),
                      ),
                      RichText(
                        text: TextSpan(
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 13,
                          ),
                          children: [
                            TextSpan(text: "${widget.previousStatus} to "),
                            TextSpan(
                              text: "${widget.newStatus}",
                              style: TextStyle(
                                color: widget.newStatus == "Danger"
                                    ? Colors.red
                                    : Colors.black,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Checkbox(
                    value: _checkBoxValue,
                    onChanged: (value) {
                      setState(() {
                        _checkBoxValue = value!;
                      });
                    })
              ],
            ),
          ),
          isThreeLine: true,
        ),
        Divider(),
      ],
    );
  }
}

class MonitoringAlertTile extends StatefulWidget {
  final String name;
  final String ward;
  final String eatAt;
  final String duration;
  final Color tilePrimaryColor;
  final bool checkboxValue;
  MonitoringAlertTile({
    Key? key,
    required this.name,
    required this.ward,
    required this.eatAt,
    required this.tilePrimaryColor,
    required this.duration,
    this.checkboxValue = false,
    //default value for checkbox
  }) : super(key: key);

  @override
  _MonitoringAlertTileState createState() =>
      _MonitoringAlertTileState(checkboxValue);
}

class _MonitoringAlertTileState extends State<MonitoringAlertTile> {
  bool _checkBoxValue;
  _MonitoringAlertTileState(this._checkBoxValue);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ListTile(
          onTap: () => Navigator.pushNamed(context, babyDetails),
          leading: CircleAvatar(
            backgroundColor: widget.tilePrimaryColor,
          ),
          title: Text("Baby of ${widget.name}"),
          subtitle: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(
                "${widget.ward} ward",
                style: TextStyle(
                  fontSize: 13,
                ),
              ),
              Text(
                "EAT ${widget.eatAt}",
                style: TextStyle(
                  fontSize: 13,
                ),
              ),
            ],
          ),
          trailing: Container(
            width: MediaQuery.of(context).size.width * 0.5,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 12),
                  child: Text(
                    "${widget.duration} Checkup",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Checkbox(
                    value: _checkBoxValue,
                    onChanged: (value) {
                      setState(() {
                        _checkBoxValue = value!;
                      });
                    })
              ],
            ),
          ),
          isThreeLine: true,
        ),
        Divider(),
      ],
    );
  }
}
