import 'package:flutter/material.dart';

class CustomAppBar extends StatelessWidget {
  final title;
  final bool drawerAvailable;
  const CustomAppBar(
      {Key? key, required this.title, required this.drawerAvailable})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 120,
      child: Padding(
        padding: const EdgeInsets.only(top: 8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(16, 0, 0, 0),
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Visibility(
                      visible: drawerAvailable,
                      child: Icon(
                        Icons.menu,
                        color: Colors.white,
                        size: 28,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 16),
                    child: RichText(
                      text: TextSpan(
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text: title,
                            style: TextStyle(
                              fontSize: 22,
                            ),
                          ),
                          TextSpan(
                            text: "\n" + "ID: *****124",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 8, 24, 0),
              child: Container(
                child: Center(
                  child: Image.asset(
                    "assets/images/icon/icon.png",
                    width: 60,
                    height: 60,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      decoration: BoxDecoration(
        color: Theme.of(context).primaryColor,
        borderRadius: BorderRadius.only(
          bottomLeft: const Radius.circular(
            25.0,
          ),
          bottomRight: const Radius.circular(
            25.0,
          ),
        ),
      ),
    );
  }
}
