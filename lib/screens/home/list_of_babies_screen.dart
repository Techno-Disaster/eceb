import 'package:eceb/utils/router.dart';
import 'package:flutter/material.dart';
import 'package:menu_button/menu_button.dart';

class ListofBabiesScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SearchBar(),
            Padding(
              padding: const EdgeInsets.only(top: 9, right: 9),
              child: SortButton(),
            ),
          ],
        ),
        SizedBox(
          height: 10,
        ),
        RecentRegisteredBabyList(),
        SizedBox(
          height: 20,
        ),
        PastRegisteredBabyList(),
      ],
    );
  }
}

class SearchBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 9.0, left: 9),
      child: Container(
        width: MediaQuery.of(context).size.width * 0.6,
        height: 50,
        decoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
          borderRadius: BorderRadius.all(
            Radius.circular(30),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Text(
              "Search the list of babies",
            ),
            Icon(
              Icons.search,
              color: Colors.white,
              size: 18,
            ),
          ],
        ),
      ),
    );
  }
}

class SortButton extends StatefulWidget {
  @override
  _SortButtonState createState() => _SortButtonState();
}

class _SortButtonState extends State<SortButton> {
  late String selectedKey;
  late String initialValue;

  List<String> keys = <String>[
    'Time',
    'Status',
    'Location',
  ];
  @override
  void initState() {
    selectedKey = keys[0];
    initialValue = keys[0];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final Widget normalChildButton = Container(
      decoration: BoxDecoration(
        color: Theme.of(context).primaryColor,
        borderRadius: BorderRadius.all(
          Radius.circular(30),
        ),
      ),
      width: MediaQuery.of(context).size.width * 0.3,
      height: 50,
      child: Padding(
        padding: const EdgeInsets.only(left: 16, right: 11),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Flexible(
              child: Text(
                selectedKey,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            SizedBox(
              width: 12,
              height: 17,
              child: FittedBox(
                fit: BoxFit.fill,
                child: Icon(Icons.sort),
              ),
            ),
          ],
        ),
      ),
    );
    return MenuButton<String>(
      itemBackgroundColor: Theme.of(context).primaryColor,
      decoration: BoxDecoration(
        color: Theme.of(context).primaryColor,
        borderRadius: BorderRadius.all(
          Radius.circular(30),
        ),
      ),
      child: normalChildButton,
      items: keys,
      selectedItem: selectedKey,
      showSelectedItemOnList: false,
      itemBuilder: (String value) => Container(
        height: 40,
        alignment: Alignment.centerLeft,
        padding: const EdgeInsets.symmetric(vertical: 0.0, horizontal: 16),
        child: Text(value),
      ),
      toggledChild: Container(
        child: normalChildButton,
      ),
      onItemSelected: (String value) {
        setState(() {
          selectedKey = value;
        });
      },
      onMenuButtonToggle: (bool isToggle) {},
    );
  }
}

class BabyListItem extends StatelessWidget {
  final String parent;
  final String ward;
  final String gender;
  final Color accentColor;
  final Color primaryColor;
  final bool multiple;
  final int index;
  BabyListItem({
    required this.parent,
    required this.ward,
    required this.gender,
    required this.accentColor,
    required this.primaryColor,
    this.multiple = false,
    this.index = 1,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5),
      child: Container(
        color: primaryColor,
        child: MaterialButton(
          elevation: 0,
          onPressed: () => Navigator.pushNamed(context, babyDetails),
          color: primaryColor,
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Container(
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 5,
                    blurRadius: 10,
                    offset: Offset(0, 2),
                  ),
                ],
                color: Colors.white,
                borderRadius: BorderRadius.all(
                  Radius.circular(20),
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: double.infinity,
                    decoration: BoxDecoration(
                      color: accentColor,
                      borderRadius: BorderRadius.all(
                        Radius.circular(20),
                      ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        "22 Minutes from Birth",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                RichText(
                                  text: TextSpan(
                                    children: <TextSpan>[
                                      TextSpan(
                                        text:
                                            multiple ? "Baby $index " : "Baby ",
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18,
                                        ),
                                      ),
                                      TextSpan(
                                        text: "of ",
                                        style: TextStyle(
                                          fontSize: 16,
                                          color: Colors.black,
                                        ),
                                      ),
                                      TextSpan(
                                        text: parent,
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: 8,
                                ),
                                RichText(
                                  text: TextSpan(
                                    style: TextStyle(
                                      fontSize: 14.0,
                                      color: Colors.black,
                                    ),
                                    children: <TextSpan>[
                                      TextSpan(text: "Location: "),
                                      TextSpan(
                                        text: ward,
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                Column(
                                  children: [
                                    Icon(
                                      Icons.person,
                                      color: Colors.black,
                                      size: 24,
                                    ),
                                    Text(
                                      gender,
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16,
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  width: 16,
                                ),
                                Icon(
                                  Icons.chevron_right_sharp,
                                  color: Colors.black,
                                  size: 30,
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class PastRegisteredBabyList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 40, bottom: 4),
          child: Text(
            "Past Registered",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 16,
            ),
          ),
        ),
        BabyListItem(
          parent: "Riya",
          ward: "Pernatal Ward",
          gender: "Female",
          accentColor: Colors.red.shade100,
          primaryColor: Colors.red,
        ),
        BabyListItem(
          parent: "Nia",
          ward: "Pernatal Ward",
          gender: "Male",
          accentColor: Colors.yellow.shade100,
          primaryColor: Colors.yellow,
        ),
        BabyListItem(
          parent: "Adisa",
          ward: "Pernatal Ward",
          gender: "Female",
          accentColor: Colors.green.shade100,
          primaryColor: Colors.green,
          multiple: true,
          index: 1,
        ),
        BabyListItem(
          parent: "Adisa",
          ward: "Pernatal Ward",
          gender: "Male",
          accentColor: Colors.green.shade100,
          primaryColor: Colors.green,
          multiple: true,
          index: 2,
        ),
        BabyListItem(
          parent: "Riya",
          ward: "Pernatal Ward",
          gender: "Female",
          accentColor: Colors.red.shade100,
          primaryColor: Colors.red,
        ),
        BabyListItem(
          parent: "Deka",
          ward: "Pernatal Ward",
          gender: "Female",
          accentColor: Colors.yellow.shade100,
          primaryColor: Colors.yellow,
        ),
      ],
    );
  }
}

class RecentRegisteredBabyList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 40, bottom: 4),
          child: Text(
            "Recently Added",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 16,
            ),
          ),
        ),
        BabyListItem(
          parent: "Oni",
          ward: "Pernatal Ward",
          gender: "Male",
          accentColor: Colors.blue.shade100,
          primaryColor: Colors.blue.shade300,
        ),
      ],
    );
  }
}
