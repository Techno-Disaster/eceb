import 'package:eceb/screens/auth/select_auth_screen.dart';
import 'package:eceb/utils/router.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    GlobalKey navBarKey = GlobalKey(debugLabel: 'navBar');
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primaryColor: Color(0xff82A0C8),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: SelectAuthScreen(
        navBarKey: navBarKey,
      ),
      onGenerateRoute: RouteGenerator.generateRoute,
    );
  }
}
