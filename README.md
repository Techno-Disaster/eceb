# About 

The Essential Care for Every Baby (ECEB) educational and training program, developed by the American Academy of Pediatrics, provides knowledge, skills, and competencies to nurses and doctors in low/middle-income settings so that they can provide life-saving care to newborns from birth through 24 hours postnatal.

## Samples


| ![](assets/samples/Screenshot_20210309-214920_eceb.png)  | ![](assets/samples/Screenshot_20210309-214722_eceb.png)  |  ![](assets/samples/Screenshot_20210309-214736_eceb.png) |
|---|---|---|
|![](assets/samples/Screenshot_20210309-214742_eceb.png)  | ![](assets/samples/Screenshot_20210309-214746_eceb.png)  | ![](assets/samples/Screenshot_20210309-214750_eceb.png)  |

## Project Structure

Widgets required for a particular screen are in their own classes in the same file, so we can change this easily to a more traditional `components` structure later
```
lib
│   ├── main.dart
│   ├── screens
│   │   ├── auth
│   │   │   ├── facility_login_screen.dart
│   │   │   ├── individual_login_screen.dart
│   │   │   ├── select_auth_screen.dart
│   │   │   └── widgets
│   │   │       ├── banners.dart
│   │   │       ├── credentials_textfield.dart
│   │   │       └── custom_select_button.dart
│   │   └── home
│   │       ├── base.dart
│   │       ├── home_screen.dart
│   │       ├── list_of_babies_screen.dart
│   │       ├── notifications_screen.dart
│   │       ├── profile_screen.dart
│   │       └── widgets
│   │           └── custom_app_drawer.dart
│   └── utils
│       └── router.dart
```
## Build and run

* To run the project install flutter and run ```flutter run``` in the project directory
* By default supports full [NNBD](https://dart.dev/null-safety), if you want to add some dependency which isn't migrated to null safety yet, run ``` flutter run --no-sound-null-safety``` in the project directory.

